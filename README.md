This is a base image for RVM. It has an entrypoint that ensures that RVM is
installed in the volume attached to /usr/local/rvm. See the Rakefile for
examples on how to use.
