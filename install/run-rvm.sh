#!/usr/bin/env bash

APP_DIR=$(pwd)
RUN_USER="rvmuser"
INSTALLED_FILE="/usr/local/rvm/installed.at"
RUBY_VERSION_TO_INSTALL="ruby-2.1.6"

if [[ ! -e $INSTALLED_FILE ]]
then
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  \curl -sSL https://get.rvm.io | bash -s stable --ruby=${RUBY_VERSION_TO_INSTALL}
fi
rvm requirements

if [[ ! -e "/etc/profile.d/rvm.sh" ]]
then
  . /usr/local/rvm/scripts/initialize
  . /usr/local/rvm/scripts/functions/logging
  . /usr/local/rvm/scripts/functions/support
  . /usr/local/rvm/scripts/functions/installer
  setup_etc_profile
fi

#bundle install
#exec bundle exec "$@"
#exec "$@"
exec runuser - $RUN_USER -c "cd ${APP_DIR} && . ./development.env &>/dev/null; $@"
