task default: :build

IMAGE="crisply/rvm"
RVM_PATH="/usr/local/rvm"
APP_PATH="/code"
FOREMAN_ARGS="-f Procfile.thin web=1,worker=0,grunt=0"

desc "Build the image from the Dockerfile"
task :build do
  img = ENV['docker_tag'] ? "#{IMAGE}:#{ENV['docker_tag']}" : IMAGE
  sh "docker build --rm=true -t #{img} ."
end

desc "Run the image"
task :run do
  args = ENV['docker_args'] ? "#{ENV['docker_args']}" : "/bin/bash -l"
  sh "docker run -it -v $(pwd)/rvm:#{RVM_PATH} #{IMAGE} -- #{args}"
end

desc "Run Rails in Development mode with foreman"
task :railsdev do
  sh <<-EOF
    docker run -it \
      -v $(pwd)/rvm:#{RVM_PATH} \
      -v #{APP_PATH}:/app \
      -w /app/crisply \
      #{IMAGE} \
      "foreman start #{FOREMAN_ARGS}"
  EOF
end

desc "Clean up containers and images"
task :clean do
  sh "docker rm $(docker ps -aq)"
  sh "docker rmi #{IMAGE}"
end
